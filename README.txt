Project 2 Summary

TestList:
	• TODO also try with a LinkedList - does it make any difference?
		○ No, all tests still pass. The methods for LinkedLists also work for ArrayLists.

TestIterator:
	• TODO also try with a LinkedList - does it make any difference?
		○ No, all tests still pass. The methods for LinkedLists also work for ArrayLists.
	• TODO what happens if you use list.remove(Integer.valueOf(77))?
		○ This is the correct code to use to make the test pass. i.remove() doesn't work because i is an iterator variable, not referring to the list. It throws an IllegalStateException.

TestPerformance:
	• Size 10; reps: 1000000; 177 ms
	• Size 100; reps 1000000; 226 ms
	• Size 1000; reps: 1000000; 690 ms
	• Size 10000; reps: 1000000; 7 seconds 850 ms
	• Size 100000; reps: 1000000;  1 min, 12 sec; ArrayLists are better at accessing elements (18 ms) and LinkedLists are better at adding and removing elements (37 ms)
	• LinkedLists overall are worse at accessing elements when the size gets larger
	• ArrayLists perform better overall
	• For LinkedLists, accessing elements requires going through all the elements in the list to reach the desired one. It is easier to add and remove elements from
		LinkedLists because it only requires changing the next and previous pointers to point to the new element. ArrayLists are better at accessing elements in
		O(n) time complexity, but worse at adding or removing elements because the entire ArrayList has to be travered first.